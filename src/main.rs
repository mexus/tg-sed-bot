extern crate telecat;

extern crate futures;
extern crate tokio;
extern crate futures_retry;

#[macro_use]
extern crate failure;

use std::sync::{Arc, Mutex};
use std::env;
use std::collections::HashMap;

use futures::Stream;
use futures_retry::{RetryPolicy, StreamRetryExt};

use telecat::StreamFlatExt;

extern crate sedregex;

#[macro_use]
extern crate slog;
extern crate slog_scope;
extern crate slog_stdlog;
extern crate slog_term;
extern crate slog_async;

#[macro_use]
extern crate log;

use std::fs::OpenOptions;
use slog::Drain;

mod processing;

fn main() {
    let log_path = "bot.log";
    let file = OpenOptions::new()
        .create(true)
        .write(true)
        .truncate(true)
        .open(log_path)
        .unwrap();

    // create logger
    use slog::Drain;
    // let decorator = slog_term::PlainSyncDecorator::new(file);
    let decorator = slog_term::TermDecorator::new().build();
    let drain = slog_term::CompactFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();

    let logger = slog::Logger::root(drain, o!());

    // slog_stdlog uses the logger from slog_scope, so set a logger there
    let _guard = slog_scope::set_global_logger(logger);

    // register slog_stdlog as the log handler with the log crate
    slog_stdlog::init().unwrap();

    info!("logger has been initialized");

    let token = env::var("TELECAT_TOKEN").expect("You must provide the bot token.");

    let bot = Arc::new(telecat::Bot::new([telecat::TG_API_URL, &token].concat()).unwrap());
    let raw_stream = telecat::RawStream::new(&bot);

    // key: (chat_id, user_msg_id)
    // value: (chat_id, bot_message_id)
    let processed_messages = Arc::new(Mutex::new(HashMap::new()));

    info!("bot is starting");

    let bot = raw_stream
        .retry(|_| RetryPolicy::Repeat)
        .flat_iter()
        .for_each(move |upd| {
            seq_handle(&*bot, &upd, processed_messages.clone());

            Ok(())
        });

    tokio::run(bot);
}

fn seq_handle(bot: &telecat::Bot, upd: &telecat::types::Update, cache: processing::ArcMsgCache) {
    processing::process_upd(bot, upd, cache.clone());
}
