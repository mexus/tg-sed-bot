use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use failure;
use telecat;

use sedregex::find_and_replace;

pub type ArcMsgCache = Arc<Mutex<HashMap<(i64, i64), (i64, i64)>>>;

pub fn process_upd(bot: &telecat::Bot, upd: &telecat::types::Update, cache: ArcMsgCache) -> Result<(), ()> {
    debug!("new update: {:?}", upd);
    if let Some(ref msg) = upd.message {
        debug!("new message income");
        let res = process_new_msg(bot, msg, cache);
        debug!("res: {:?}", res);
    } else if let Some(ref msg) = upd.edited_message {
        debug!("edited message income");
        let res = process_edited_msg(bot, msg, cache);
        debug!("res: {:?}", res);
    } else {
        debug!("no message income");
        return Err(());
    }

    Ok(())
}

fn process_new_msg(bot: &telecat::Bot, msg: &telecat::types::Message, cache: ArcMsgCache) {
    let chat_id = msg.chat.id;

    let text_to_process = match msg.reply_to_message {
        Some(ref boxed_message) => match boxed_message.text {
            Some(ref text) => text,
            None => {
                warn!("Skipping: parent msg doesn't contain text");
                return
            },
        },
        None => {
            debug!("Skipping: msg is not a reply");
            return
        },
    };

    let msg_to_reply_id = match msg.reply_to_message {
        Some(ref boxed_message) => boxed_message.message_id,
        None => {
            warn!("Skipping: parent msg doesn't contain message_id field");
            return
        },
    };

    let sed_commands = match msg.text {
        Some(ref text) => text,
        None => {
            warn!("Skipping: child msg (reply) doesn't contain text");
            return
        },
    };

    match process(&text_to_process, &sed_commands) {
        Ok(result) => {
            if result.is_empty() {
                let sent_msg = match bot.send_message::<telecat::types::NullMarkup>(
                    &chat_id, "(empty message)",
                    None, None, None, Some(&msg_to_reply_id), None
                ){
                    Ok(msg) => msg,
                    Err(e) => {
                        error!("Error while sending msg: {:?}", e);
                        return
                    },
                };

                let mut cache_ = cache.lock().unwrap();
                cache_.insert(
                    (chat_id, msg.message_id), (chat_id, sent_msg.message_id)
                );
            } else {
                let sent_msg = match bot.send_message::<telecat::types::NullMarkup>(
                    &chat_id, &result,
                    None, None, None, Some(&msg_to_reply_id), None
                ){
                    Ok(msg) => msg,
                    Err(e) => {
                        error!("Error while sending msg: {:?}", e);
                        return
                    },
                };

                let mut cache_ = cache.lock().unwrap();
                cache_.insert(
                    (chat_id, msg.message_id), (chat_id, sent_msg.message_id)
                );
            }
        },
        Err(e) => {
            warn!("Error while processing message: {:?}", e);
            return
        }
    }
}

fn process_edited_msg(bot: &telecat::Bot, msg: &telecat::types::Message, cache: ArcMsgCache) -> Result<(), failure::Error> {
    let (chat_id, msg_to_edit_id) = {
        match cache.lock().unwrap().get(&(msg.chat.id, msg.message_id)) {
            Some(tuple) => tuple.clone(),
            None => {

                return Err(format_err!("Skipping: msg not found in history"));
            },
        }
    };

    let text_to_process = match msg.reply_to_message {
        Some(ref boxed_message) => match boxed_message.text {
            Some(ref text) => text,
            None => return Err(format_err!("Skipping: parent msg doesn't contain text")),
        },
        None => return Err(format_err!("Skipping: parent msg doesn't contain msg?")),
    };

    let sed_commands = match msg.text {
        Some(ref text) => text,
        None => return Err(format_err!("Skipping: child msg (reply) doesn't contain text")),
    };

    match process(&text_to_process, &sed_commands) {
        Ok(result) => {
            if result.is_empty() {
                let _edited_msg = match bot.edit_message(
                    &chat_id, Some(&msg_to_edit_id), None, "(empty message)", None, None // , None
                ){
                    Ok(msg) => msg,
                    Err(_) => {
                        error!("Error while sending msg");
                        return Err(format_err!("Error while editing message"))
                    },
                };
            } else {
                let _edited_msg = match bot.edit_message(
                    &chat_id, Some(&msg_to_edit_id), None, &result, None, None // , None
                ){
                    Ok(msg) => msg,
                    Err(_) => {
                        error!("Error while sending msg");
                        return Err(format_err!("Error while editing message"))
                    },
                };
            }
        },
       Err(e) => {
            return Err(format_err!("Error while processing message: {:?}", e));
        }
    }

    Ok(())
}

pub fn process(msg: &String, instructions: &String) -> Result<String, failure::Error> {
    let commands = instructions.split('\n')
                               .filter_map(|p| preprocess_pattern(p).ok())
                               .collect::<Vec<String>>();

    if commands.is_empty() {
        return Err(format_err!("Skipping: no commands"));
    }

    let text = match find_and_replace(&msg, &commands) {
        Ok(t) => t,
        Err(e) => return Err(format_err!("Error while evaluating substitution: {:?}", e)),
    };

    Ok(text.to_string())
}

fn preprocess_pattern(line: &str) -> Result<String, failure::Error> {
    println!("pattern in preproc: {}", line);
    if !(line.starts_with("s/") || line.starts_with("/s/")) {
        return Err(format_err!("Text is not a pattern"));
    }

    let t = if line.starts_with("/") {
        &line[1..]
    } else {
        &line
    };

    let n_slashes = t.matches("/").count();
    let n_escaped_slashes = t.matches("\\/").count();

    let mut tt = String::from(t);

    if (n_slashes - n_escaped_slashes) == 2 {
        tt.push('/');
    }

    println!("after preproc: {}", tt);
    Ok(tt)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn preprocessing_not_valid() {
        let input = "sblah";
        let actual = preprocess_pattern(input);
        actual.unwrap();
    }

    #[test]
    #[should_panic]
    fn preprocessing_empty() {
        let input = "";
        let actual = preprocess_pattern(input);
        actual.unwrap();
    }


    #[test]
    fn preprocessing_short() {
        let input = "s/a/b";
        let actual = preprocess_pattern(input);
        let expected = "s/a/b/";
        assert_eq!(actual.unwrap(), expected);
    }

    #[test]
    fn preprocessing_short_with_leading_slash() {
        let input = "/s/a/b";
        let actual = preprocess_pattern(input);
        let expected = "s/a/b/";
        assert_eq!(actual.unwrap(), expected);
    }

    #[test]
    fn preprocessing_full() {
        let input = "s/a/b/";
        let actual = preprocess_pattern(input);
        println!("{:#?}", actual);
        let expected = "s/a/b/";
        assert_eq!(actual.unwrap(), expected);
    }

    #[test]
    fn preprocessing_full_with_leading_slash() {
        let input = "/s/a/b/";
        let actual = preprocess_pattern(input);
        println!("{:#?}", actual);
        let expected = "s/a/b/";
        assert_eq!(actual.unwrap(), expected);
    }

    #[test]
    fn process_simple_1() {
        let input = "a very short text";
        let commands = "s/\\w+/_/g";
        let expected = "_ _ _ _";
        let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
        assert_eq!(expected, actual);
    }

    #[test]
    fn process_simple_2() {
        let input = "a$ very%% sh@ort! tex\\w+t";
        let commands = "s/\\w+/_/g";
        let expected = "_$ _%% _@_! _\\_+_";
        let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
        assert_eq!(expected, actual);
    }

    #[test]
    fn process_simple_3() {
        let input = "a$ very%% sh@ort! tex\\w+t";
        let commands = "s/\\w+/_/";
        let expected = "_$ very%% sh@ort! tex\\w+t";
        let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
        assert_eq!(expected, actual);
    }

    // NOTE: This test is deprecated since the substitution is based on regex library.
    // #[test]
    // fn process_multiline_text_1() {
    //     let input = "a very short text\nwith some adittional lines\nand another one";
    //     let commands = "s/\\w+/_/";
    //     let expected = "_ very short text\n_ some adittional lines\n_ another one";
    //     let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
    //     assert_eq!(expected, actual);
    // }

    #[test]
    fn process_multiline_text_2() {
        let input = r#"Lorem Ipsum is simply dummy text
of the printing and typesetting industry. Lorem Ipsum
has been the industry's standard dummy text ever since
 the 1500s, when an unknown printer took a galley of
type and scrambled it to make a type specimen book.
It has"#;
        let commands = "s/\\w+/_/g";
        let expected = "_ _ _ _ _ _\n_ _ _ _ _ _. _ _\n_ _ _ _\'_ _ _ _ _ _\n _ _, _ _ _ _ _ _ _ _\n_ _ _ _ _ _ _ _ _ _.\n_ _";
        let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
        assert_eq!(expected, actual);
    }

    #[test]
    fn process_multiline_text_multicommand_1() {
        let input = r#"Lorem Ipsum is simply dummy text
of the printing and typesetting industry. Lorem Ipsum
has been the industry's standard dummy text ever since
 the 1500s, when an unknown printer took a galley of
type and scrambled it to make a type specimen book.
It has"#;
        let commands = r#"s/[a-z]+/_/g
s/[0-9+]/=/g
s/_//g
s/ +//g"#;
        let expected = "LI\n.LI\n\'\n====,\n.\nI";
        let actual = process(&input.to_string(), &commands.to_string()).expect("Error processing data");
        assert_eq!(expected, actual);
    }

}
