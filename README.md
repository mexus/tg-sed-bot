# Sed Substitution bot for Telegram

This bot processes the message with provided sed-like commands in a
reply. Also, it can edit own messages if the reply has been updated,
giving you the ability to fix a possible error among the commands.

## Usage

Bot is based on telecat, so you should provide `TELECAT_TOKEN`
environment variable with telegram bot token

After the set up process, go to BotFather again, and give your bot the
pirmission to read all messages in the chats. This will significantly
improve the experience with bot, since you can use simplified
commands, as discribed below.

## Commands

The following commands considered the same:

* `s/pattern/replacement/` - common, replaces the first entry only
* `s/pattern/replacement` - without trailing slash
* `/s/pattern/replacement/` - with leading slash
* `/s/pattern/replacement` - with leading slash & without trailing slash

So if you want to use additional flags, the last slash is required:

* `s/pattern/replacement/gi` - replaces all entries, case-insensitive

the available flags are:

* `g` - replace every matching entry 
* `i` - consider pattern case-insensitive
* `U` - greedy swap
* `x` - ignore whitespaces

## Features

The most unique feature of this bot is that you can edit your message
with commands, so bot will re-process that message again, and then
will update the corresponding reply message according to the recent
changes.

